<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/member/list', 'MemberController@index')->name('member.list');
Route::get('/member/create', 'MemberController@create')->name('member.create');
Route::post('/create_member', 'MemberController@create_member')->name('member.create_member');
Route::get('/member/delete/{id}', 'MemberController@delete')->name('member.delete');
Route::get('/member/edit/{id}', 'MemberController@edit')->name('member.edit');