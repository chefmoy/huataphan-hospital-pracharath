  <?php
  	$segment1 = \Request::segment(1);
  	$segment2 = \Request::segment(2);

  ?>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">เมนูหลัก</li>
        <li class="treeview <?php echo $segment1 == "member" ? "active menu-open" : "" ?>">
          <a href="#">
            <i class="fa fa-folder"></i> <span>สมาชิก</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="<?php echo $segment1 == "member" && $segment2 == "list" ? "active" : "" ?>"><a href="/member/list"><i class="fa fa-circle-o"></i> รายการสมาชิก</a></li>
            <li class="<?php echo $segment1 == "member" && $segment2 == "create" ? "active" : "" ?>"><a href="/member/create"><i class="fa fa-circle-o"></i> สร้างสมาชิก</a></li>
          </ul>
        </li>
      
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>