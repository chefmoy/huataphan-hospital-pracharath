
<!-- Bootstrap 3.3.7 -->
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/bower_components/fastclick/lib/fastclick.js"></script>
<!-- bootstrap datepicker -->
<script src="/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="/dist/js/demo.js"></script>
<!-- page script -->

<script src="/js/js/bootstrap-datepicker.js"></script>
<script src="/js/js/bootstrap-datepicker-thai.js"></script>
<script src="/js/js/locales/bootstrap-datepicker.th.js"></script>

<script>

  //Date picker
  $('#birth_day').datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
  })
   //Date picker
  $('#register_date').datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
  })

    //Date picker
  $('#expire_date').datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
  })

      //Date picker
  $('#start_date').datepicker({
    format: 'dd-mm-yyyy',
    autoclose: true,
  })

  $(function () {
    $('#example1').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : true,
      'ordering'    : false,
      'info'        : false,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
