
@include('auth.include.header')
<div class="login-box">
  <div class="login-logo">
     <a>ลงชื่อเข้าใช้งาน</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">กรอกชื่อผู้ใช้และรหัสผ่าน</p>

      <form method="POST" action="{{ route('login') }}">
        @csrf
      <div class="form-group has-feedback">
        <input id="username" type="username"  class="form-control @error('username') is-invalid @enderror" placeholder="username" name="username" value="{{ old('username') }}">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
          @error('username')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
      </div>
      <div class="form-group has-feedback">
       <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
      </div>

    <div class="social-auth-links text-center">
           <button type="submit" class="btn btn-block btn-primary">
                 ลงชื่อเข้าใช้
          </button>
    </div>
    <!-- /.social-auth-links -->
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
@include('auth.include.footer')
