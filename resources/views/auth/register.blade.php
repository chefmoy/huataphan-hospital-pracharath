@include('auth.include.header')
<div class="register-box">
  <div class="register-logo">
    <a>ลงทะเบียนเข้าใช้งาน</a>
  </div>

    <div class="register-box-body">
      <p class="login-box-msg">กรอกข้อมูลผู้ใช้งาน</p>

      <form method="POST" action="{{ route('register') }}">
        @csrf

          <div class="form-group has-feedback">
             <input  id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}"  autocomplete="name"  placeholder="FirstName - LastName">
             @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
          </div>


         <div class="form-group has-feedback">
          <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}"  autocomplete="username" placeholder="Username">
             @error('username')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group has-feedback">
          <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}"  autocomplete="email" placeholder="Email">
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group has-feedback">
          <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password" placeholder="Password">
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="form-group has-feedback">
           <input id="password-confirm" type="password" class="form-control" name="password_confirmation"  autocomplete="new-password" placeholder="Retype Password">

        </div>

      <div class="social-auth-links text-center">
        
           <button type="submit" class="btn btn-block btn-primary">
                  ลงทะเบียน
          </button>

      </div>

      </form>
    </div>
</div>
@include('auth.include.footer')
