@extends('layouts.default')

@section('content')
    	   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        สมาชิก
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
       @if ($message = Session::get('success'))
      <div class="callout callout-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      @if ($errors->any())
      <div class="callout callout-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
      <div class="row">
        <div class="col-xs-12">

          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">รายการสมาชิก</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>ชื่อ - สกุล</th>
                  <th>วัน เดือน ปีเกิด</th>
                  <th>สถานะ</th>
                  <th>วันที่สมัคร</th>
                  <th>เริ่มใช้สิทธิ</th>
                  <th>วันหมดอายุ</th>
                  <th>จัดการ</th>
                </tr>
                </thead>
                <tbody>
                  @if(count($list) > 0)
                    @foreach ($list as $key => $value)
              				<tr>
              					<td>{{ $value->title. " " . $value->firstname. " " .  $value->lastname }}</td>
              					<td>{{ $value->birth_day }}</td>
                        <td> 
                            @if($value->alive == 1)
                              <span class="badge badge-success" style="background-color: #337ab7;">ยังมีชีวิตอยู่</span>
                            @else   
                              <span class="badge badge-danger" style="background-color: #d73925;">เสียชีวิตแล้ว</span>
                            @endif                     
                        </td>
              					<td>{{ $value->register_date }}</td>
              					<td>{{ $value->start_date }}</td>
              					<td>{{ $value->expire_date }}</td>
                        <td>
                            <a  href = "/member/edit/{{ $value->id }}"  class="btn btn-success">แก้ไข</a>
                            <a  href = "/member/delete/{{ $value->id }}" onclick='return confirm("คุณแน่ใจหรือว่าจะลบ {{ $value->title. ' ' . $value->firstname. ' ' .  $value->lastname }} ออกจากระบบ")'  class="btn btn-danger">ลบ</a>
                        </td>
              				</tr>
                    @endforeach
                    @endif
                </tbody>
                <tfoot>
                <tr>
                   <th>ชื่อ - สกุล</th>
                  <th>วัน เดือน ปีเกิด</th>
                  <th>สถานะ</th>
                  <th>วันที่สมัคร</th>
                  <th>เริ่มใช้สิทธิ</th>
                  <th>วันหมดอายุ</th>
                  <th>จัดการ</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
@endsection