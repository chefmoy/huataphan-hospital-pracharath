@extends('layouts.default')

@section('content')
    	   <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        สมาชิก
      </h1>
    </section>

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">สร้างสมาชิก</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id = "member_form" role="form" method="post" action="{{url('create_member')}}">
              {{ csrf_field() }}
              <input type="hidden" name="id_edit" id = "id_edit" value="<?php echo isset($list['id']) ? $list['id'] : "" ?>" />
              <div class="col-md-6">
                <div class="box-body">
                   <!-- select -->
                  <div class="form-group">
                    <label>คำนำหน้าชื่อ</label><span class="validate-warning-pin">*</span>
                    <select class="form-control" id = "name_prefix" name = "name_prefix">
                      <option value="">กรุณาเลือกคำนำหน้าชื่อ</option>
                      @foreach($listTitle as $key => $row)
                        @if(isset($list['id']))
                          <option  <?php echo $list['name_title_id'] == $row['id']  ? "selected" : "" ?> value="{{ $row['id'] }}" >{{ $row['title'] }}</option>
                        @else
                          <option value="{{ $row['id'] }}" >{{ $row['title'] }}</option>
                        @endif
                      @endforeach
                    </select>
                    <span class="validate-warning" id = "name_prefix-error">กรุณาเลือก คำนำหน้าชื่อ</span>
                  </div>
                    <div class="form-group">
                    <label for="lastname">นามสกุล</label><span class="validate-warning-pin">*</span>
                    <input value = "<?php echo isset($list['lastname']) ? $list['lastname'] : "" ?>" type="text" class="form-control" placeholder="นามสกุล" id = "lastname" name = "lastname">
                     <span class="validate-warning" id = "lastname-error">กรุณากรอก lastname</span>
                  </div>
                 <!-- radio -->
                  <div class="form-group" style="margin-top: 29px;">
                    <div class="radio">
                      <label>
                        <input type="radio" name="alive" id="alive" value="1" checked>
                        ยังมีชีวิตอยู่
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="alive" id="die" value="0"  >
                        เสียชีวิตแล้ว
                      </label>
                    </div>
                  </div>  
                      
                  <div class="form-group" id = "birth_day_group">
                  <label for="birth_day">วัน-เดือน-ปีเกิด</label><span class="validate-warning-pin">*</span>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input readonly  data-provide="datepicker" data-date-language="th-th" type="text" class="form-control" placeholder="วัน-เดือน-ปีเกิด" id = "birth_day" name = "birth_day" value = "<?php echo isset($list['birth_day']) ? $list['birth_day'] : "" ?>">
                    </div>
                    <span class="validate-warning" id = "birth_day-error">กรุณาเลือก วัน-เดือน-ปีเกิด</span>
                  </div>

                  <div class="form-group">
                    <label for="register_date">วันที่สมัคร</label><span class="validate-warning-pin">*</span>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                       <input readonly  data-provide="datepicker" data-date-language="th-th" type="text" class="form-control" placeholder="วันที่สมัคร" id = "register_date" name = "register_date" value = "<?php echo isset($list['register_date']) ? $list['register_date'] : "" ?>">
                    </div>
                    <span class="validate-warning" id = "register_date-error">กรุณาเลือก วันที่สมัคร</span>
                   
                  </div>
                  <div class="form-group">
                    <label for="expire_date">วันหมดอายุ</label><span class="validate-warning-pin">*</span>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                       <input readonly  data-provide="datepicker" data-date-language="th-th" type="text" class="form-control" placeholder="วันหมดอายุ" id = "expire_date" name = "expire_date" value = "<?php echo isset($list['expire_date']) ? $list['expire_date'] : "" ?>">
                    </div>
                     <span class="validate-warning" id = "expire_date-error">กรุณาเลือก วันหมดอายุ</span>
                    
                  </div>
                 <div class="form-group">
                    <label for="amount">จำนวนเงิน</label><span class="validate-warning-pin">*</span>
                    <input type="text" class="form-control" placeholder="จำนวนเงิน" id = "amount" name = "amount" value = "<?php echo isset($list['amount']) ? $list['amount'] : "" ?>">
                    <span class="validate-warning" id = "amount-error">กรุณากรอก จำนวนเงิน</span>
                  </div>
                </div>
              </div>
                <!-- /.box-body -->
              <div class="col-md-6">
                    <div class="box-body">
                       <div class="form-group">
                        <label for="firstname">ชื่อ</label><span class="validate-warning-pin">*</span>
                        <input type="text" class="form-control" placeholder="ชื่อ" id = "firstname" name = "firstname" value = "<?php echo isset($list['firstname']) ? $list['firstname'] : "" ?>">
                        <span class="validate-warning" id = "firstname-error">กรุณากรอก ชื่อ</span>
                      </div>
                        <div class="form-group">
                        <label for="thai_id">เลขที่บัตรประชาชน</label><span class="validate-warning-pin">*</span>
                        <input onfocusout="ValidateThaiID()" onkeyup="checkLimit('thai_id')" type="number" class="form-control" placeholder="เลขที่บัตรประชาชน" id = "thai_id" name = "thai_id" value = "<?php echo isset($list['thai_id']) ? $list['thai_id'] : "" ?>">
                         <span class="validate-warning" id = "thai_id-error">กรุณากรอก เลขที่บัตรประชาชน</span>
                      </div>
                  <div class="form-group">
                    <label for="hn_number">เลขที่ HN</label>
                    <input type="text" class="form-control" placeholder="เลขที่ HN" id = "hn_number" name = "hn_number" value = "<?php echo isset($list['hn_number']) ? $list['hn_number'] : "" ?>">
                  </div>
                  <div class="form-group">
                    <label for="member_number">เลขที่สมาชิก</label><span class="validate-warning-pin">*</span>
                    <input type="text" class="form-control" placeholder="เลขที่สมาชิก" id = "member_number" name = "member_number" value = "<?php echo isset($list['member_number']) ? $list['member_number'] : "" ?>">
                    <span class="validate-warning" id = "member_number-error">กรุณากรอก เลขที่สมาชิก</span>
                  </div>
                  <div class="form-group">
                    <label for="start_date">วันเริ่มใช้สิทธิ</label><span class="validate-warning-pin">*</span>
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input readonly  data-provide="datepicker" data-date-language="th-th" type="text" class="form-control" placeholder="วันเริ่มใช้สิทธิ" id = "start_date" name = "start_date" value = "<?php echo isset($list['start_date']) ? $list['start_date'] : "" ?>">
                    </div>
                    <span class="validate-warning" id = "start_date-error">กรุณาเลือก วันเริ่มใช้สิทธิ</span>
                  </div>
                 <div class="form-group">
                    <label for="amount">เลขที่ใบเสร็จ</label><span class="validate-warning-pin">*</span>
                    <input type="text" class="form-control" placeholder="เลขที่ใบเสร็จ" id = "receipt_number" name = "receipt_number" value = "<?php echo isset($list['receipt_number']) ? $list['receipt_number'] : "" ?>">
                    <span class="validate-warning" id = "receipt_number-error">กรุณากรอก เลขที่ใบเสร็จ</span>
                  </div>
                       <!-- textarea -->
                  <div class="form-group">
                    <label>หมายเหตุ</label>
                    <textarea id = "remark" name = "remark" class="form-control" rows="3" placeholder="หมายเหตุ"><?php echo isset($list['remark']) ? $list['remark'] : "" ?></textarea>
                  </div>
                    </div>
              </div> 
                 <div class="box-footer">
                  <a href = "#" class="btn btn-primary" id = "save">บันทึก</a>
                  <a href = "/member/list" class="btn btn-warning">ยกเลิก</a>
                </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>

  <script type="text/javascript">
    
    $(document).ready(function() {
        var item = "<?php echo isset($list['alive']) ? $list['alive'] : ""  ?>"
        if(item != "" && item == 0) {
           $("#die").click()
        }
    })

    function ValidateThaiID() {
        var str = $("#thai_id").val()
        var maxLength = 13;
        if(str.length < maxLength) {
          alert("เลขที่บัตรประชาชนน้อยกว่า 13 หลกกรุณากรอกให้ครบ")
          $("#thai_id").val("")
          return false
        }
    }

     function checkLimit(id) {
       var str = $("#" + id).val()
       var maxLength = 13;
       if(str.length > maxLength) {
        $("#" + id).val(str.substring(0, maxLength))
        return false
       }
     }

      var ignoreFeild = ['hn_number', 'remark', 'birth_day']

      $('input[name=alive]').click(function() {
          var selValue = $('input[name=alive]:checked').val(); 
          if(selValue == 0) {
             $("#birth_day_group").hide(200)
          } else {
             $("#birth_day_group").show(200)
          }
      });

      $("#save").click(function() {
        var list = []
        $("form#member_form :input").each(function(){
          if($(this).attr("id") != undefined && $(this).attr("id") != "hn_number" && $(this).attr("id") != "remark" && $(this).attr("id") != "birth_day" && $(this).attr("id") != "id_edit") {
            if($("#" + $(this).attr("id")).val() == "" || $("#" + $(this).attr("id")).val() == "") {
                $("#" + $(this).attr("id") + "-error").show()
                $("#" + $(this).attr("id")).addClass("require-warning")
                list.push($(this).attr("id"))
            } else {
                $("#" + $(this).attr("id") + "-error").hide()
                $("#" + $(this).attr("id")).removeClass("require-warning")
            }
          }
        });

        let selValue = $('input[name=alive]:checked').val(); 
        if(selValue == 1 && $("#birth_day").val() == "") {
             $("#birth_day-error").show()
             $("#birth_day").addClass("require-warning")
             list.push($(this).attr("birth_day"))
        } else {
             $("#birth_day-error").hide()
             $("#birth_day").removeClass("require-warning")
        }
        console.log(list)
        if(list.length > 0) {
           return false
        } else {
          $("#member_form").submit();
        }
      })
  </script>
@endsection