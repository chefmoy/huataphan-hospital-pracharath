<?php

namespace App;

date_default_timezone_set("Asia/Bangkok");

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Member extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string
     */
    protected $table = 'member';

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_title_id', 'firstname', 'lastname', 'thai_id', 'alive', 'hn_number', 'birth_day', 'member_number', 'register_date', 'start_date', 'expire_date', 'receipt_number', 'amount', 'remark'
    ];

    /**
     * Get the phone record associated with the member.
     */
    public static function getListMember()
    {
        return \DB::table('member')
            ->select("member.*", "name_title.title")
            ->leftJoin('name_title', 'member.name_title_id', '=', 'name_title.id')
            ->where('member.deleted_at', null)
            ->orderBy('member.created_at', 'desc')
            ->get();
    }
    /**
     * Get the phone record associated with the member.
     */
    public static function getListMemberMyID($id)
    {
        return self::where('id', $id)->get();
    }

}
