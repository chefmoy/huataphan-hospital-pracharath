<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Member;
Use App\NameTitle;
use Validator,Redirect,Response;


class MemberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        try {
            $data['list'] = Member::getListMember()->toArray();
            return view('member.list', $data);
        } catch (\Exception $e) {
            return Redirect::to("member/list")->withSuccess($e->getMessage());
        }

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit($id)
    {   
        try {
            $data['listTitle'] = NameTitle::getAllTitle();
            $data['list'] = Member::getListMemberMyID($id)->toArray()[0];
            return view('member.create', $data);
        } catch (\Exception $e) {
            return Redirect::to("member/list")->withSuccess($e->getMessage());
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function delete($id)
    {   
        try {
            Member::where('id', $id)->delete();
            return Redirect::to("member/list")->withSuccess('ลบข้อมูลเรียบร้อยแล้ว');
        } catch (\Exception $e) {
            return Redirect::to("member/list")->withSuccess($e->getMessage());
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {   
        try {
            $data['listTitle'] = NameTitle::getAllTitle();
            return view('member.create', $data);
        } catch (\Exception $e) {
            return Redirect::to("member/list")->withSuccess($e->getMessage());
        }
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create_member(Request $request)
    {
        $data = $request->all();

        if(isset($data['id_edit']) && $data['id_edit'] != "") {
          $this->update_item($data, $data['id_edit']); 
          return Redirect::to("member/list")->withSuccess('แก้ไขข้อมูลเรียบร้อยแล้ว');  
        } else {
          $this->create_item($data);
          return Redirect::to("member/list")->withSuccess('บันทึกข้อมูลเรียบร้อยแล้ว');  
        }
    }

    public function update_item(array $data, $id) {
      return Member::where('id', $id)->update($this->setFeild($data));
    }

    public function create_item(array $data) {
      return Member::create($this->setFeild($data));
    }

    public function setFeild(array $data) {
        return [
         'name_title_id' => $data['name_prefix'],
         'firstname' =>$data['firstname'],
         'lastname' => $data['lastname'],
         'thai_id' => $data['thai_id'],
         'alive' => $data['alive'],
         'hn_number' => $data['hn_number'],
         'birth_day' => $data['birth_day'],
         'member_number' => $data['member_number'],
         'register_date' => $data['register_date'],
         'start_date' => $data['start_date'],
         'expire_date' => $data['expire_date'],
         'receipt_number' => $data['receipt_number'],
         'amount' => $data['amount'],
         'remark' => $data['remark'],
      ];
    }
}
