<?php

namespace App;

date_default_timezone_set("Asia/Bangkok");

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class NameTitle extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var string
     */
    protected $table = 'name_title';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title'
    ];

    /**
     * Get the user that owns the phone.
     */
    public function member()
    {
        return $this->belongsTo('App\Member');
    }

    /**
     * Get the phone record associated with the user.
     */
    public static function getAllTitle()
    {
        return self::select('*')->get();
    }

    
}
